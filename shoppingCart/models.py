from django.contrib.auth.models import User
from django.db import models

# Create your models here.

class Products(models.Model):
    title=models.CharField(max_length=30)
    description = models.TextField(max_length=100)
    image=models.ImageField(upload_to='templates/images', null=True,blank=True)
    price=models.IntegerField()
    created_At = models.DateField(auto_now_add=True)
    updated_At = models.DateField(auto_now=True)

    def __str__(self):
        return f'{self.title}'

class Orders(models.Model):
    PAYMENT_CHOICES = (
        ("cash", "cash"),
        ("paytm", "paytm"),
        ("card", "mastercard"),
        ("phonepay", "phonepay"),

    )
    STATUS_CHOICES=(
        ('new','new'),
        ('paid','paid')
    )

    user= models.ForeignKey(User,on_delete=models.CASCADE,null=True, blank=True)
    total = models.FloatField()
    products = models.ForeignKey(Products,on_delete=models.CASCADE, null=True, blank=True)
    created_At = models.DateField(auto_now_add=True)
    updated_At = models.DateField(auto_now=True)
    status= models.CharField(max_length=20,choices=STATUS_CHOICES)
    mode_of_payment=models.CharField(max_length=20,choices=PAYMENT_CHOICES)

    def __str__(self):
        return f"{self.id}"
   
class OrdersItems(models.Model):
    orders=models.ForeignKey(User,on_delete=models.CASCADE,default=None)
    products=models.ForeignKey(Products,on_delete=models.CASCADE)
    Quantity=models.IntegerField()


    def price(self):
        return self.product.price
    
