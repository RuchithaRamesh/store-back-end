# from django.db import models
from .models import Orders,Products,OrdersItems
from rest_framework import serializers
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token

class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Products
        fields = [  'id',
                    'title' ,
                    'description' ,
                    'price' ,
                    'created_At' ,
                    'updated_At','image']

    def create(self,**validatedata):
        return Products.objects.create(**validatedata)

    def update(self, instance, validated_data):
        instance.title = validated_data.get('title', instance.title)
        instance.description = validated_data.get('description', instance.description)
        instance.image = validated_data.get('image', instance.image)
        instance.price = validated_data.get('price', instance.price)
        instance.created_At = validated_data.get('created_At', instance.created_At)
        instance.updated_At = validated_data.get('updated_At', instance.updated_At)
        instance.save()
        return instance


class OrdersSerializer(serializers.ModelSerializer):
    class Meta:
        model = Orders
        fields = [
            'id',
            'user_id',
            'total',
            'products',
            'created_At',
            'updated_At',
            'status',
            'mode_of_payment',

        ]
        depth=1

class Order_items_Serializer(serializers.ModelSerializer):
    class Meta:
        model = OrdersItems
        fields = [
            'id',
            'orders_id',
            'products_id',
            'Quantity',
        ]
        depth = 2


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username', 'password']
        extra_kwargs = {'password': {'write_only': True, 'required': True}}

    def create(self, validated_data):
        user = User.objects.create_user(**validated_data)
        Token.objects.create(user=user)
        return user



